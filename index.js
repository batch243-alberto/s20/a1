/*
console.log("The number you provided is 100.");
let count = 100;

while(count>=50) {
	console.log("The number is divisible by 5. Skipping the number");

	count -=5;
	console.log(count);
}*/

console.log("The number you provided is 100.");
for (let count = 100; count>0; count-=5){

		if(count <=50 ){
			console.log("The current " + 50 + " . Terminating the loop ")
			break;
		}
		else if(count % 10 === 0){
				console.log("The number is divisible by 10. Skipping the number ");
		continue;		
	}
		console.log(count);
	}
let word = "supercalifragilisticexpialidocious";

let wordWithOutVowel="";

for(let i = 0; i < word.length; i++){
	if(word[i].toLowerCase() === "a" || 
			word[i].toLowerCase() === "e" || 
			word[i].toLowerCase() === "i" || 
			word[i].toLowerCase() === "o" ||
			word[i].toLowerCase() === "u"){
		continue;
	}
	else{
		wordWithOutVowel += word[i];
	}
}
console.log(wordWithOutVowel);

